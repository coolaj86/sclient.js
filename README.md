sclient.js
==========

Secure Client for exposing TLS (aka SSL) secured services as plain-text connections locally.

Also ideal for multiplexing a single port with multiple protocols using SNI.

Unwrap a TLS connection:

```bash
$ sclient whatever.com:443 localhost:3000
> [listening] whatever.com:443 <= localhost:3000
```

Connect via Telnet

```bash
$ telnet localhost 3000
```

Connect via netcat (nc)

```bash
$ nc localhost 3000
```

cURL

```bash
$ curl http://localhost:3000 -H 'Host: whatever.com'
```

Inverse SSH proxy (ssh over https):

```bash
$ sclient ssh user@example.com
```

(this is the same as a normal SSH Proxy, just easier to type):

```bash
$ ssh -o ProxyCommand="sclient %h" user@example.com
```

Inverse rsync proxy (rsync over https):

```bash
$ sclient rsync user@example.com:path/ path/
```

A poor man's (or Windows user's) makeshift replacement for `openssl s_client`, `stunnel`, or `socat`.

Install
=======

### macOS, Linux, Windows

First download and install the *current* version of [node.js](https://nodejs.org)

```bash
npm install -g sclient
```

```bash
npx sclient example.com:443 localhost:3000
```

Usage
=====

```bash
sclient [flags] [ssh|rsync] <remote> [local]
```

* flags
  * `-k, --insecure` ignore invalid TLS (SSL/HTTPS) certificates
  * `--servername <string>` spoof SNI (to disable use IP as &lt;remote&gt; and do not use this option)
* remote
  * must have servername (i.e. example.com)
  * port is optional (default is 443)
* local
  * address is optional (default is localhost)
  * must have port (i.e. 3000)

Examples
========

Bridge between `telebit.cloud` and local port `3000`.

```bash
sclient telebit.cloud 3000
```

Same as above, but more explicit

```bash
sclient telebit.cloud:443 localhost:3000
```

Ignore a bad TLS/SSL/HTTPS certificate and connect anyway.

```bash
sclient -k badtls.telebit.cloud:443 localhost:3000
```

### Reading from stdin

```bash
sclient telebit.cloud:443 -
```

```bash
sclient telebit.cloud:443 - </path/to/file
```

### ssh over https

```bash
sclient ssh user@telebit.cloud
```

### rsync over https

```bash
sclient rsync -av user@telebit.cloud:my-project/ ~/my-project/
```

### Piping

```bash
printf "GET / HTTP/1.1\r\nHost: telebit.cloud\r\n\r\n" | sclient telebit.cloud:443
```

Testing for security vulnerabilities on the remote:

```bash
sclient --servername "Robert'); DROP TABLE Students;" -k example.com localhost:3000
```

```bash
sclient --servername "../../../.hidden/private.txt" -k example.com localhost:3000
```
